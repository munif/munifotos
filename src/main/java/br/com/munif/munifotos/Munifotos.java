/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.munifotos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author munif
 */
public class Munifotos {

    private final File source;
    private final File destination;
    private final List<File> toSearch;
    private final MetaDataReader md;

    public Munifotos(File source, File destination) {
        this.md = new MetaDataReader();
        this.source = source;
        this.destination = destination;
        toSearch = new ArrayList<>();
    }

    public void start() {
        int i = 0;
        int nPhotos = 0;
        toSearch.addAll(Arrays.asList(source.listFiles()));
        while (i < toSearch.size()) {
            File f = toSearch.get(i++);
            if (f.isDirectory()) {
                toSearch.addAll(Arrays.asList(f.listFiles()));
                continue;
            }
            if (isPhoto(f)) {
                nPhotos++;
                GraphicScreen.get().displayMessageInTitle("Searching... " + nPhotos + "/" + i + "/" + toSearch.size() + " " + f.getAbsolutePath());
                //GraphicScreen.get().setImage(f);
                String ff = md.readMetadata(f);
                File out = new File(destination.getAbsolutePath() + "/" + ff);
                File dirOut = out.getParentFile();
                dirOut.mkdirs();
                copyFile(f, out);
            }
        }
        GraphicScreen.get().displayMessageInTitle("Searched " + i + " files and found " + nPhotos + " photos.");
        System.exit(0);
    }

    private boolean isPhoto(File f) {
        if (f.isDirectory()) {
            return false;
        }
        if (f.length() < 1024 * 100) {
            return false;
        }
        String fileName = f.getName();

        if (fileName.toUpperCase().endsWith(".JPG")) {
            return true;
        }
        return false;
    }

    public static void copyFile(File in, File out) {
        System.out.print(in.getAbsolutePath()+" "+out.getAbsolutePath()+" ");
        if (out.exists()) {
            if (out.length() >= in.length()) {
                System.out.println(" already in place.");
                return;
            }
        }
        try {
            FileChannel inChannel = new FileInputStream(in).getChannel();
            FileChannel outChannel = new FileOutputStream(out).getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
            if (inChannel != null) {
                inChannel.close();
            } else {
            }
            if (outChannel != null) {
                outChannel.close();
            }
            System.out.println(" copied.");
        } catch (Exception ex) {
            System.out.println(" problem.");
            ex.printStackTrace();
        }
        
    }
}
