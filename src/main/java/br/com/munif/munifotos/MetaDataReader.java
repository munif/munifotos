package br.com.munif.munifotos;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifDirectory;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import org.w3c.dom.*;

public class MetaDataReader {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd");

    public String readMetadata(File file) {
        String aRetornar = "";
        try {
            Metadata metadata = ImageMetadataReader.readMetadata(file);
            // Read Exif Data
            Directory directory = metadata.getDirectory(ExifDirectory.class);
            if (directory != null) {
                Date date = directory.getDate(ExifDirectory.TAG_DATETIME);
                aRetornar = sdf.format(date) + "/" + file.getName();
                return aRetornar;
            }
        } catch (Exception e) {
            //System.out.println("Problem reading metadata of "+file.getAbsolutePath());
            //e.printStackTrace();
        }
        return "nodate/"+file.getName();
    }
}
