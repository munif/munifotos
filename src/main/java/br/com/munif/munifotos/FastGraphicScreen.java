/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.munifotos;

import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author munif
 */
public class FastGraphicScreen extends JFrame {

    private static FastGraphicScreen instance;
    private final String TITLE;
    private BufferedImage img;

    public static synchronized FastGraphicScreen get() {
        if (instance == null) {
            instance = new FastGraphicScreen();
        }
        return instance;
    }

    private FastGraphicScreen() {
        img = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);
        this.TITLE = "Munifotos 2.0 ";
        setTitle(TITLE);
        setSize(800, 600);
        setVisible(true);
        add(new JPanel() {

            @Override
            public void paint(Graphics g) {
                //super.paint(g); //To change body of generated methods, choose Tools | Templates.
                g.drawImage(img, 0, 0, rootPane);
            }

        });

        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void displayMessageInTitle(String message) {
        setTitle(TITLE + " " + message);
    }

    public void setImage(File f) {
        try {
            img = ImageIO.read(f);
            int largura = img.getWidth();
            int altura = img.getHeight();
            Image scaledInstance = null;
            if (largura > altura) {
                scaledInstance = img.getScaledInstance(this.getWidth() - 10, -1, BufferedImage.SCALE_REPLICATE);
            } else {
                scaledInstance = img.getScaledInstance(-1, this.getHeight() - 20, BufferedImage.SCALE_REPLICATE);
            }
            //img.getGraphics().drawImage(scaledInstance, 0, 0, rootPane);
            //SwingUtilities.updateComponentTreeUI(this);
        } catch (IOException ex) {
            Logger.getLogger(FastGraphicScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
