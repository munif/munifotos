/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.munifotos;

import java.io.File;

/**
 *
 * @author munif
 */
public class Programa {

    public static void main(String[] args) {
        titulo();
        if (args.length != 2) {
            System.out.println("java -jar munifotos.jar source destination");
            System.exit(1);
        }
        File source = new File(args[0]);
        File destination = new File(args[1]);
        if (!source.isDirectory()) {
            System.out.println("Source must be a directory.");
            System.exit(2);
        }
        if (destination.exists() && !destination.isDirectory()) {
            System.out.println("Destination can't be a file.");
        }
        System.out.println(source.getAbsolutePath() + " --->  " + destination.getAbsolutePath());
        new Munifotos(source,destination).start();
        
        
    }

    public static void titulo() {
        System.out.println(""
                + "    __  ___            _ ____      __            \n"
                + "   /  |/  /_  ______  (_) __/___  / /_____  _____\n"
                + "  / /|_/ / / / / __ \\/ / /_/ __ \\/ __/ __ \\/ ___/\n"
                + " / /  / / /_/ / / / / / __/ /_/ / /_/ /_/ (__  ) \n"
                + "/_/  /_/\\__,_/_/ /_/_/_/  \\____/\\__/\\____/____/  \n"
                + "                                                 \n"
                + "Munif Gebara Jr. www.munif.com.br munif@munif.com.br\n"
                + "");
    }

}
