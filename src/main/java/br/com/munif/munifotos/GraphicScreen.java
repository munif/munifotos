/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.munif.munifotos;

import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 *
 * @author munif
 */
public class GraphicScreen extends JFrame {

    private static GraphicScreen instance;
    private final String TITLE;
    private final JLabel imagem;

    public static synchronized GraphicScreen get() {
        if (instance == null) {
            instance = new GraphicScreen();
        }
        return instance;
    }

    private GraphicScreen() {
        setLayout(new FlowLayout(FlowLayout.CENTER));
        this.TITLE = "Munifotos 2.0 ";
        setTitle(TITLE);
        setSize(800, 600);
        imagem = new JLabel();
        imagem.setHorizontalTextPosition(JLabel.CENTER);
        imagem.setVerticalTextPosition(JLabel.TOP);
        add(imagem);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void displayMessageInTitle(String message) {
        setTitle(TITLE + " " + message);
        //System.out.println(message);

    }

    public void setImage(File f) {
        try {
            BufferedImage img = ImageIO.read(f);
            int largura = img.getWidth();
            int altura = img.getHeight();
            Image scaledInstance = null;
            if (largura > altura) {
                scaledInstance = img.getScaledInstance(this.getWidth()-10, -1, BufferedImage.SCALE_REPLICATE);
            } else {
                scaledInstance = img.getScaledInstance(-1, this.getHeight() - 20, BufferedImage.SCALE_REPLICATE);
            }

            imagem.setIcon(new ImageIcon(scaledInstance));
            SwingUtilities.updateComponentTreeUI(imagem);
        } catch (IOException ex) {
            Logger.getLogger(GraphicScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
